//
//  ViewController.m
//  AVSyncTest
//
//  Created by Thomas Guillem on 25/01/2023.
//

#import "ViewController.h"
#import "SampleBufferRendererView.h"
#import "vlc-clock.h"
#import "vlc-output.h"

#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVAssetReader.h>
#import <AVFoundation/AVAssetTrack.h>
#import <AVFoundation/AVAssetReaderOutput.h>

@interface ViewController ()
@property (weak, nonatomic) IBOutlet SampleBufferRendererView *rendererView;
@end

@implementation ViewController {
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [self inputThread];
    });
}

static void displayCallback(void *ctx, CMSampleBufferRef buffer)
{
    SampleBufferRendererView *view = (__bridge SampleBufferRendererView *)ctx;

    dispatch_async(dispatch_get_main_queue(), ^{
        [view.displayLayer enqueueSampleBuffer:buffer];
        CFRelease(buffer);
    });
}

- (void) inputThread {
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"Sync-One2_Test_1080p_29.97_H.264_AAC_Stereo" withExtension:@"mov"];
    assert(url);

    AVAsset *asset = [AVAsset assetWithURL:url];
    assert(asset);

    AVAssetReader *assetReader = [[AVAssetReader alloc] initWithAsset:asset error:nil];
    assert(assetReader);

    NSArray* videoTracks = [asset tracksWithMediaType:AVMediaTypeVideo];
    assert(videoTracks);
    AVAssetTrack* videoTrack = [videoTracks objectAtIndex:0];

    NSDictionary *videoDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [NSNumber numberWithInt:kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange],
                                     (NSString*)kCVPixelBufferPixelFormatTypeKey, nil];

    AVAssetReaderTrackOutput* assetReaderVideoOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:videoTrack
                                                                                        outputSettings:videoDictionary];
    assert(assetReaderVideoOutput);
    [assetReader addOutput:assetReaderVideoOutput];

    NSArray* audioTracks = [asset tracksWithMediaType:AVMediaTypeAudio];
    assert(audioTracks);
    AVAssetTrack* audioTrack = [audioTracks objectAtIndex:0];

    NSDictionary *audioDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                     [NSNumber numberWithInt:kAudioFormatLinearPCM], AVFormatIDKey,
                                     [NSNumber numberWithFloat:44100.0], AVSampleRateKey,
                                     [NSNumber numberWithInt:32], AVLinearPCMBitDepthKey,
                                     [NSNumber numberWithBool:NO], AVLinearPCMIsNonInterleaved,
                                     [NSNumber numberWithBool:YES], AVLinearPCMIsFloatKey,
                                     [NSNumber numberWithBool:NO], AVLinearPCMIsBigEndianKey,
                                     nil];

    AVAssetReaderTrackOutput* assetReaderAudioOutput = [[AVAssetReaderTrackOutput alloc] initWithTrack:audioTrack
                                                                                        outputSettings:audioDictionary];
    assert(assetReaderAudioOutput);
    [assetReader addOutput:assetReaderAudioOutput];

    [assetReader startReading];

    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback
                                            mode:AVAudioSessionModeMoviePlayback
                              routeSharingPolicy:AVAudioSessionRouteSharingPolicyLongFormAudio
                                         options:0
                                           error:nil];

    vlc_output *output = vlc_output_New();
    assert(output);

    int ret;
    ret = vlc_output_ConfigureVideo(output, displayCallback, (__bridge void *)self.rendererView);
    assert(ret == 0);

    ret = vlc_output_ConfigureAudio(output);
    assert(ret == 0);

    int renderFlag = VLC_OUTPUT_RENDER_ALL;
    while (true) {
        CMSampleBufferRef audioBuffer = NULL, videoBuffer = NULL;

        if (renderFlag & VLC_OUTPUT_RENDER_AUDIO) {
            audioBuffer = [assetReaderAudioOutput copyNextSampleBuffer];
            if (audioBuffer != NULL)
                vlc_output_Render(output, audioBuffer, &renderFlag);
        }

        if (renderFlag & VLC_OUTPUT_RENDER_VIDEO) {
            videoBuffer = [assetReaderVideoOutput copyNextSampleBuffer];
            if (videoBuffer != NULL)
                vlc_output_Render(output, videoBuffer, &renderFlag);
        }

        if (audioBuffer == NULL && videoBuffer == NULL)
            break;
    }

    vlc_output_Delete(output);
}

@end
