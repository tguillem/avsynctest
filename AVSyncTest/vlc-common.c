/*****************************************************************************
 * vlc_common.h: common definitions
 * Collection of useful common types and macros definitions
 *****************************************************************************
 * Copyright (C) 1998-2011 VLC authors and VideoLAN
 *
 * Authors: Samuel Hocevar <sam@via.ecp.fr>
 *          Vincent Seguin <seguin@via.ecp.fr>
 *          Gildas Bazin <gbazin@videolan.org>
 *          Rémi Denis-Courmont
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/


#include <time.h>
#include "vlc-common.h"

void vlc_mutex_init(vlc_mutex_t *mtx)
{
    pthread_mutex_init(mtx, NULL);
}

void vlc_mutex_destroy(vlc_mutex_t *mtx)
{
    pthread_mutex_destroy(mtx);
}

int vlc_mutex_lock(vlc_mutex_t *mtx)
{
    return pthread_mutex_lock(mtx);
}

int vlc_mutex_unlock(vlc_mutex_t *mtx)
{
    return pthread_mutex_unlock(mtx);
}

void vlc_cond_init(vlc_cond_t *cond)
{
    pthread_cond_init(cond, NULL);
}

void vlc_cond_destroy(vlc_cond_t *cond)
{
    pthread_cond_destroy(cond);
}

void vlc_cond_broadcast(vlc_cond_t *cond)
{
    pthread_cond_broadcast(cond);
}

void vlc_cond_signal(vlc_cond_t *cond)
{
    pthread_cond_signal(cond);
}

void vlc_cond_wait(vlc_cond_t *cond, vlc_mutex_t *mtx)
{
    pthread_cond_wait(cond, mtx);
}

static struct timespec *vlc_tick_to_timespec(struct timespec *restrict ts,
                                             vlc_tick_t tick)
{
    lldiv_t d = lldiv(tick, CLOCK_FREQ);

    ts->tv_sec = d.quot;
    ts->tv_nsec = NS_FROM_VLC_TICK(d.rem);
    return ts;
}

int vlc_cond_timedwait(vlc_cond_t *cond, vlc_mutex_t *mtx, vlc_tick_t deadline)
{
    struct timespec ts;

    vlc_tick_to_timespec(&ts, deadline - vlc_tick_now());
    return pthread_cond_timedwait_relative_np(cond, mtx, &ts);
}

void vlc_tick_sleep (vlc_tick_t delay)
{
    struct timespec ts;

    vlc_tick_to_timespec(&ts, delay);
    while (nanosleep(&ts, &ts) == EINTR);
}

vlc_tick_t vlc_tick_now (void)
{
    struct timespec ts;

    if (unlikely(clock_gettime(CLOCK_MONOTONIC, &ts) != 0))
        abort();

    return vlc_tick_from_timespec( &ts );
}

