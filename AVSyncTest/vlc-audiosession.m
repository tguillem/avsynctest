/*****************************************************************************
 * coreaudio_common.c: Common AudioUnit code for iOS and macOS
 *****************************************************************************
 * Copyright (C) 2005 - 2017 VLC authors and VideoLAN
 *
 * Authors: Derk-Jan Hartman <hartman at videolan dot org>
 *          Felix Paul Kühne <fkuehne at videolan dot org>
 *          David Fuhrmann <david dot fuhrmann at googlemail dot com>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#include <CoreAudio/CoreAudioTypes.h>
#include "vlc-common.h"
#include "vlc-audiosession.h"

#include <AudioUnit/AudioUnit.h>
#include <AudioToolbox/AudioToolbox.h>
#include <os/lock.h>
#include <mach/mach_time.h>
#import <CoreAudio/CoreAudioTypes.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

struct vlc_aout
{
    mach_timebase_info_data_t tinfo;

    bool                b_played;
    CMSimpleQueueRef    queue;
    CMSampleBufferRef   current_buffer;
    size_t              current_offset;
    /* Size written via the render callback */
    uint64_t            i_total_bytes;
    /* Bytes written since the last timing report */
    size_t timing_report_last_written_bytes;
    /* Number of bytes to write before sending a timing report */
    size_t timing_report_delay_bytes;
    /* AudioUnit Latency */
    vlc_tick_t au_latency_ticks;

    vlc_clock_t *clock;

    int                 i_rate;
    unsigned int        i_bytes_per_frame;

    AVAudioSession *avInstance;
    /* The AudioUnit we use */
    AudioUnit au_unit;

    /* For debug purpose, to print when specific latency changed */
    vlc_tick_t output_latency_ticks;
    vlc_tick_t io_buffer_duration_ticks;
};


#define TIMING_REPORT_DELAY_TICKS VLC_TICK_FROM_MS(1000)

static inline uint64_t
BytesToFrames(vlc_aout *aout, size_t i_bytes)
{
    return i_bytes / aout->i_bytes_per_frame;
}

static inline vlc_tick_t
FramesToTicks(vlc_aout *aout, int64_t i_nb_frames)
{
    return vlc_tick_from_samples(i_nb_frames, aout->i_rate);
}

static inline vlc_tick_t
BytesToTicks(vlc_aout *aout, size_t i_bytes)
{
    return FramesToTicks(aout, BytesToFrames(aout, i_bytes));
}

static inline size_t
FramesToBytes(vlc_aout *aout, uint64_t i_frames)
{
    return i_frames * aout->i_bytes_per_frame;
}

static inline int64_t
TicksToFrames(vlc_aout *aout, vlc_tick_t i_ticks)
{
    return samples_from_vlc_tick(i_ticks, aout->i_rate);
}

static inline size_t
TicksToBytes(vlc_aout *aout, vlc_tick_t i_ticks)
{
    return FramesToBytes(aout, TicksToFrames(aout, i_ticks));
}

static inline vlc_tick_t
HostTimeToTick(vlc_aout *aout, int64_t i_host_time)
{
    return VLC_TICK_FROM_NS(i_host_time * aout->tinfo.numer / aout->tinfo.denom);
}

static vlc_tick_t
GetLatency(vlc_aout *aout)
{
    vlc_tick_t latency_us = 0, us;
    bool changed = false;

    us = vlc_tick_from_secf([aout->avInstance outputLatency]);
    if (us != aout->output_latency_ticks)
    {
        fprintf(stderr, "Current device has a new outputLatency of %" PRId64 "us\n", us);
        aout->output_latency_ticks = us;
        changed = true;
    }
    latency_us += us;

    us = vlc_tick_from_secf([aout->avInstance IOBufferDuration]);
    if (us != aout->io_buffer_duration_ticks)
    {
        fprintf(stderr, "Current device has a new IOBufferDuration of %" PRId64 "us\n", us);
        aout->io_buffer_duration_ticks = us;
        changed = true;
    }
    latency_us += us;

    latency_us += aout->au_latency_ticks;

    if (changed)
        fprintf(stderr, "Current device has a new total latency of %" PRId64 "us\n",
                latency_us);
    return latency_us;
}

static void
Render(vlc_aout *aout, uint64_t host_time, uint8_t *data, size_t bytes)
{
    vlc_tick_t host_delay_ticks = 0;
    if (host_time != 0)
    {
        /* Convert the absolute host time to a relative delay in ticks (us) */
        uint64_t now_nsec = clock_gettime_nsec_np(CLOCK_UPTIME_RAW);
        host_delay_ticks = HostTimeToTick(aout, host_time)
                         - VLC_TICK_FROM_NS(now_nsec);
    }
    const vlc_tick_t bytes_ticks = BytesToTicks(aout, bytes);

    const vlc_tick_t now_ticks = vlc_tick_now();
    const vlc_tick_t end_ticks = now_ticks + bytes_ticks + host_delay_ticks;

    size_t bytes_copied = 0;
    while (bytes > 0)
    {
        CMSampleBufferRef buffer = aout->current_buffer ? aout->current_buffer
            : (CMSampleBufferRef) CMSimpleQueueDequeue(aout->queue);

        if (buffer == NULL)
        {
            memset(data, 0, bytes);
            fprintf(stderr, "underrun of %lld us\n", BytesToTicks(aout, bytes));
            return;
        }

        CMBlockBufferRef block = CMSampleBufferGetDataBuffer(buffer);
        assert(block != NULL);

        size_t length_at_offset, total_length;
        char *block_data;
        OSStatus status =
            CMBlockBufferGetDataPointer(block, aout->current_offset,
                                        &length_at_offset, &total_length,
                                        &block_data);
        if (status != noErr)
        {
            memset(data, 0, bytes);
            fprintf(stderr, "CMBlockBufferGetDataPointer failed: %d\n", status);
            return;
        }

        size_t tocopy = length_at_offset > bytes ? bytes : length_at_offset;

        aout->i_total_bytes += tocopy;

        memcpy(data, block_data, tocopy);

        data += tocopy;
        bytes -= tocopy;
        aout->current_offset += tocopy;
        bytes_copied += tocopy;

        if (aout->current_offset == total_length)
        {
            /* The current buffer is fully consumed */
            CFRelease(buffer);
            aout->current_buffer = NULL;
            aout->current_offset = 0;
        }
        else
            aout->current_buffer = buffer;
    }

    /* Report the delay + latency, every 1 seconds */
    if (aout->timing_report_last_written_bytes >=
        aout->timing_report_delay_bytes)
    {
        aout->timing_report_last_written_bytes = 0;
        vlc_tick_t pos_ticks = BytesToTicks(aout, aout->i_total_bytes);

        vlc_clock_Update(aout->clock, end_ticks + GetLatency(aout), pos_ticks, 1.0);
    }
    else
        aout->timing_report_last_written_bytes += bytes_copied;
}

void
vlc_aout_Play(vlc_aout *aout, CMSampleBufferRef buffer)
{
    while (CMSimpleQueueEnqueue(aout->queue, buffer) == kCMSimpleQueueError_QueueIsFull)
    {
        CMBlockBufferRef block = CMSampleBufferGetDataBuffer(buffer);
        assert(block != NULL);
        size_t bytes = CMBlockBufferGetDataLength(block);
        vlc_tick_sleep(BytesToTicks(aout, bytes));
    }
}

AudioUnit
au_NewOutputInstance(vlc_aout *aout, OSType comp_sub_type)
{
    AudioComponentDescription desc = {
        .componentType = kAudioUnitType_Output,
        .componentSubType = comp_sub_type,
        .componentManufacturer = kAudioUnitManufacturer_Apple,
        .componentFlags = 0,
        .componentFlagsMask = 0,
    };

    AudioComponent au_component;
    au_component = AudioComponentFindNext(NULL, &desc);
    if (au_component == NULL)
    {
        fprintf(stderr, "cannot find any AudioComponent, PCM output failed\n");
        return NULL;
    }

    AudioUnit au;
    OSStatus err = AudioComponentInstanceNew(au_component, &au);
    if (err != noErr)
    {
        fprintf(stderr, "cannot open AudioComponent, PCM output failed\n");
        return NULL;
    }
    return au;
}

static OSStatus
RenderCallback(void *data, AudioUnitRenderActionFlags *ioActionFlags,
               const AudioTimeStamp *inTimeStamp, UInt32 inBusNumber,
               UInt32 inNumberFrames, AudioBufferList *ioData)
{
    vlc_aout *aout = data;

    assert(inNumberFrames == BytesToFrames(aout, ioData->mBuffers[0].mDataByteSize));
    (void)inNumberFrames;

    uint64_t i_host_time = (inTimeStamp->mFlags & kAudioTimeStampHostTimeValid)
                         ? inTimeStamp->mHostTime : 0;

    Render(aout, i_host_time, ioData->mBuffers[0].mData,
           ioData->mBuffers[0].mDataByteSize);

    return noErr;
}

static int
au_Initialize(vlc_aout *aout)
{
    AudioUnit au = aout->au_unit;

    /* Set the desired format */
    AudioStreamBasicDescription desc;
    desc.mFormatFlags = kAudioFormatFlagsNativeFloatPacked;
    desc.mChannelsPerFrame = 2;
    desc.mBitsPerChannel = 32;

    desc.mSampleRate = 44100;
    desc.mFormatID = kAudioFormatLinearPCM;
    desc.mFramesPerPacket = 1;
    desc.mBytesPerFrame = desc.mBitsPerChannel * desc.mChannelsPerFrame / 8;
    desc.mBytesPerPacket = desc.mBytesPerFrame * desc.mFramesPerPacket;

    OSStatus err = AudioUnitSetProperty(au, kAudioUnitProperty_StreamFormat,
                                        kAudioUnitScope_Input, 0, &desc,
                                        sizeof(desc));
    if (err != noErr)
    {
        fprintf(stderr, "failed to set stream format\n");
        return -1;
    }

    /* Set the IOproc callback */
    const AURenderCallbackStruct callback = {
        .inputProc = RenderCallback,
        .inputProcRefCon = aout,
    };

    err = AudioUnitSetProperty(au, kAudioUnitProperty_SetRenderCallback,
                               kAudioUnitScope_Input, 0, &callback,
                               sizeof(callback));
    if (err != noErr)
    {
        fprintf(stderr, "failed to setup render callback\n");
        return -1;
    }

    /* AU init */
    err = AudioUnitInitialize(au);

    if (err != noErr)
    {
        fprintf(stderr, "AudioUnitInitialize failed\n");
        return -1;
    }

    aout->au_latency_ticks = 0;
    aout->i_total_bytes = 0;

    aout->i_rate = desc.mSampleRate;
    aout->i_bytes_per_frame = desc.mBytesPerFrame;

    aout->timing_report_last_written_bytes =
    aout->timing_report_delay_bytes = TicksToBytes(aout, TIMING_REPORT_DELAY_TICKS);

    aout->b_played = false;

    Float64 unit_s;
    if (AudioUnitGetProperty(au, kAudioUnitProperty_Latency,
                             kAudioUnitScope_Global, 0, &unit_s,
                             &(UInt32) { sizeof(unit_s) }) == noErr)
    {
        aout->au_latency_ticks = vlc_tick_from_secf(unit_s);
        fprintf(stderr, "AudioUnit latency: %" PRId64 "us\n", aout->au_latency_ticks);
    }

    err = AudioOutputUnitStart(aout->au_unit);
    if (err != noErr)
    {
        fprintf(stderr, "AudioOutputUnitStart failed\n");
        err = AudioUnitUninitialize(au);
        if (err != noErr)
            fprintf(stderr, "AudioUnitUninitialize failed\n");
        return -1;
    }

    return 0;
}

void
vlc_aout_Destroy(vlc_aout *aout)
{
    OSStatus err = AudioOutputUnitStop(aout->au_unit);
    if (err != noErr)
        fprintf(stderr, "AudioOutputUnitStop failed");

    err = AudioUnitUninitialize(aout->au_unit);
    if (err != noErr)
        fprintf(stderr, "AudioUnitUninitialize failed\n");

    CMSampleBufferRef buffer;
    while ((buffer = (CMSampleBufferRef) CMSimpleQueueDequeue(aout->queue)) != NULL)
        CFRelease(buffer);
    if (aout->current_buffer)
    {
        CFRelease(aout->current_buffer);
        aout->current_buffer = NULL;
    }

    err = AudioComponentInstanceDispose(aout->au_unit);
    if (err != noErr)
        fprintf(stderr, "AudioComponentInstanceDispose failed");

    CFRelease(aout->queue);
    free(aout);
}

vlc_aout *
vlc_aout_New(vlc_clock_t *clock)
{
    vlc_aout *aout = calloc(1, sizeof (*aout));
    if (unlikely(aout == NULL))
        return NULL;

    aout->avInstance = [AVAudioSession sharedInstance];
    assert(aout->avInstance != NULL);

    if (mach_timebase_info(&aout->tinfo) != KERN_SUCCESS)
        return NULL;
    assert(aout->tinfo.denom != 0 && aout->tinfo.numer != 0);

    aout->clock = clock;
    aout->current_buffer = NULL;
    aout->current_offset = 0;

    aout->au_unit = NULL;
    aout->output_latency_ticks = 0;
    aout->io_buffer_duration_ticks = 0;

    OSStatus err = CMSimpleQueueCreate(NULL, 64, &aout->queue);
    if (err != 0)
    {
        free(aout);
        return NULL;
    }

    BOOL success = [aout->avInstance setPreferredSampleRate:44100 error:nil];
    if (!success)
    {
        fprintf(stderr, "failed to set preferred sample rate\n");
        goto error;
    }

    aout->au_unit = au_NewOutputInstance(aout, kAudioUnitSubType_RemoteIO);
    if (aout->au_unit == NULL)
        goto error;

    err = AudioUnitSetProperty(aout->au_unit,
                               kAudioOutputUnitProperty_EnableIO,
                               kAudioUnitScope_Output, 0,
                               &(UInt32){ 1 }, sizeof(UInt32));
    if (err != noErr)
        fprintf(stderr, "failed to set IO mode\n");

    int ret = au_Initialize(aout);
    if (ret != 0)
        goto error;

    return aout;

error:
    if (aout->au_unit != NULL)
        AudioComponentInstanceDispose(aout->au_unit);
    CFRelease(aout->queue);
    free(aout);
    return NULL;
}
