//
//  AppDelegate.h
//  AVSyncTest
//
//  Created by Thomas Guillem on 25/01/2023.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

