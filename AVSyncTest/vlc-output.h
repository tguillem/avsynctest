#ifndef VLC_OUTPUT_H
# define VLC_OUTPUT_H 1

#include "vlc-common.h"

#include <CoreMedia/CoreMedia.h>

typedef struct vlc_output vlc_output;

typedef void (*display_cb)(void *display_ctx, CMSampleBufferRef buffer);

vlc_output *vlc_output_New(void);
void vlc_output_Delete(vlc_output *out);

#define VLC_OUTPUT_RENDER_VIDEO 0x1
#define VLC_OUTPUT_RENDER_AUDIO 0x2
#define VLC_OUTPUT_RENDER_ALL (VLC_OUTPUT_RENDER_VIDEO|VLC_OUTPUT_RENDER_AUDIO)
void vlc_output_Render(vlc_output *out, CMSampleBufferRef buffer, int *render_flag);

int vlc_output_ConfigureVideo(vlc_output *out, display_cb display,
                              void *display_ctx);

int vlc_output_ConfigureAudio(vlc_output *out);

#endif
