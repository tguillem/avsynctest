//
//  SampleBufferRendererView.m
//  AVSyncTest
//
//  Created by Thomas Guillem on 25/01/2023.
//

#import <Foundation/Foundation.h>
#import "SampleBufferRendererView.h"

@implementation SampleBufferRendererView

+ (Class)layerClass {
    return [AVSampleBufferDisplayLayer class];
}

- (AVSampleBufferDisplayLayer *)displayLayer {
    return (AVSampleBufferDisplayLayer *)self.layer;
}

@end
