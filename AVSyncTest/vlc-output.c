#include "vlc-output.h"
#include "vlc-clock.h"
#include "vlc-audiosession.h"

#include <pthread.h>

struct video_out
{
    vlc_clock_t *clock;
    vlc_tick_t last_pts;
    pthread_t th;
    bool started;

    vlc_mutex_t lock;
    vlc_cond_t thread_cond;
    vlc_cond_t input_cond;

    CMSimpleQueueRef queue;

    display_cb display_cb;
    void *display_ctx;
};

struct audio_out
{
    vlc_clock_t *clock;
    vlc_tick_t last_pts;
    vlc_aout *aout;
};

struct vlc_output
{
    vlc_clock_main_t *main_clock;
    bool clock_setup;

    struct video_out video_out;
    struct audio_out audio_out;
};

static int audio_out_Init(struct audio_out *aout)
{
    aout->clock = NULL;
    aout->last_pts = VLC_TICK_INVALID;

    return 0;
}

static void audio_out_Destroy(struct audio_out *aout)
{
    if (aout->aout != NULL)
        vlc_aout_Destroy(aout->aout);

    if (aout->clock != NULL)
        vlc_clock_Delete(aout->clock);
}

static int video_out_Init(struct video_out *vout)
{
    vout->clock = NULL;
    vout->last_pts = VLC_TICK_INVALID;
    vout->started = false;
    vout->display_cb = NULL;
    vout->display_ctx = NULL;

    vlc_mutex_init(&vout->lock);
    vlc_cond_init(&vout->thread_cond);
    vlc_cond_init(&vout->input_cond);

    /* 180 buffers, that is 3sec @ 60fps, need to be higher than the biggest
     * audio latency (2secs) */
    OSStatus status = CMSimpleQueueCreate(NULL, 180, &vout->queue);
    if (status != 0)
        return -1;

    return 0;
}

static void video_out_Destroy(struct video_out *vout)
{
    if (vout->started)
    {
        vlc_mutex_lock(&vout->lock);
        vout->started = false;
        vlc_cond_signal(&vout->thread_cond);
        vlc_mutex_unlock(&vout->lock);

        pthread_join(vout->th, NULL);
    }

    if (vout->clock != NULL)
        vlc_clock_Delete(vout->clock);

    CFRelease(vout->queue);

    vlc_mutex_destroy(&vout->lock);
    vlc_cond_destroy(&vout->thread_cond);
    vlc_cond_destroy(&vout->input_cond);
}

static vlc_tick_t CMTimeToTick(CMTime timestamp)
{
    CMTime scaled = CMTimeConvertScale(
            timestamp, CLOCK_FREQ,
            kCMTimeRoundingMethod_Default);

    return scaled.value;
}

static void *video_out_Thread(void *ctx)
{
    struct video_out *vout = ctx;

    for (;;)
    {
        vlc_mutex_lock(&vout->lock);

        while (vout->started && CMSimpleQueueGetCount(vout->queue) == 0)
            vlc_cond_wait(&vout->thread_cond, &vout->lock);
        if (!vout->started)
        {
            CMSampleBufferRef buffer;
            while ((buffer = (CMSampleBufferRef) CMSimpleQueueDequeue(vout->queue)) != NULL)
                CFRelease(buffer);

            vlc_mutex_unlock(&vout->lock);
            return NULL;
        }

        CMSampleBufferRef buffer = (CMSampleBufferRef) CMSimpleQueueDequeue(vout->queue);
        vlc_cond_signal(&vout->input_cond);

        vlc_mutex_unlock(&vout->lock);

        assert(buffer != NULL);

        CMTime ts = CMSampleBufferGetPresentationTimeStamp(buffer);
        vlc_tick_t ts_tick = CMTimeToTick(ts);
        vlc_tick_t system_now = vlc_tick_now();

        vlc_tick_t system_deadline =
            vlc_clock_ConvertToSystem(vout->clock, system_now, ts_tick, 1.0);

        int ret = 0;
        vlc_clock_Lock(vout->clock);
        while (ret == 0)
            ret = vlc_clock_Wait(vout->clock, system_deadline);
        vlc_clock_Unlock(vout->clock);

        if (vout->display_cb != NULL)
            vout->display_cb(vout->display_ctx, buffer);
        else
            CFRelease(buffer);

        vlc_clock_Update(vout->clock, vlc_tick_now(), ts_tick, 1.0);
    }

    return NULL;
}

static void video_out_EnqueueBuffer(struct video_out *vout, CMSampleBufferRef buffer)
{
    vlc_mutex_lock(&vout->lock);
    while (CMSimpleQueueEnqueue(vout->queue, buffer) == kCMSimpleQueueError_QueueIsFull)
        vlc_cond_wait(&vout->input_cond, &vout->lock);
    vlc_cond_signal(&vout->thread_cond);
    vlc_mutex_unlock(&vout->lock);
}

vlc_output *vlc_output_New(void)
{
    vlc_output *out = malloc(sizeof(*out));
    if (out == NULL)
        return NULL;

    out->main_clock = vlc_clock_main_New();
    if (out->main_clock == NULL)
    {
        free(out);
        return NULL;
    }

    int ret;
    ret = video_out_Init(&out->video_out);
    if (ret != 0)
    {
        vlc_clock_main_Delete(out->main_clock);
        free(out);
        return NULL;
    }

    ret = audio_out_Init(&out->audio_out);
    if (ret != 0)
    {
        video_out_Destroy(&out->video_out);
        vlc_clock_main_Delete(out->main_clock);
        free(out);
        return NULL;
    }

    out->clock_setup = false;

    return out;
}

void vlc_output_Delete(vlc_output *out)
{
    video_out_Destroy(&out->video_out);
    audio_out_Destroy(&out->audio_out);

    vlc_clock_main_Delete(out->main_clock);
    free(out);
}

int vlc_output_ConfigureVideo(vlc_output *out, display_cb display_cb,
                              void *display_ctx)
{
    struct video_out *vout = &out->video_out;

    if (vout->clock != NULL)
        return -1;

    out->video_out.display_cb = display_cb;
    out->video_out.display_ctx = display_ctx;

    vout->clock = vlc_clock_main_CreateSlave(out->main_clock, NULL);

    if (vout->clock == NULL)
        return -1;

    int ret = pthread_create(&vout->th, NULL, video_out_Thread, vout);
    if (ret != 0)
    {
        vlc_clock_Delete(vout->clock);
        vout->clock = NULL;
        return ret;
    }
    vout->started = true;

    return 0;
}

int vlc_output_ConfigureAudio(vlc_output *out)
{
    struct audio_out *aout = &out->audio_out;

    if (aout->clock != NULL)
        return -1;

    aout->clock = vlc_clock_main_CreateMaster(out->main_clock, NULL);
    if (aout->clock == NULL)
        return -1;

    aout->aout = vlc_aout_New(aout->clock);
    if (aout->aout == NULL)
    {
        vlc_clock_Delete(aout->clock);
        return -1;
    }

    return 0;
}

void vlc_output_Render(vlc_output *out, CMSampleBufferRef buffer, int *render_flag)
{
    CMTime time = CMSampleBufferGetPresentationTimeStamp(buffer);
    vlc_tick_t tick = CMTimeToTick(time);

    if (!out->clock_setup)
    {
        vlc_clock_main_SetFirstPcr(out->main_clock, vlc_tick_now(), tick);
        out->clock_setup = true;
    }

    CMFormatDescriptionRef desc = CMSampleBufferGetFormatDescription(buffer);
    CMMediaType type = CMFormatDescriptionGetMediaType(desc);

    switch (type)
    {
        case kCMMediaType_Video:
        {
            video_out_EnqueueBuffer(&out->video_out, buffer);
            out->video_out.last_pts = tick;
            if (out->audio_out.last_pts != VLC_TICK_INVALID &&
                out->video_out.last_pts - out->audio_out.last_pts > VLC_TICK_FROM_SEC(3))
                *render_flag = VLC_OUTPUT_RENDER_AUDIO;
            break;
        }
        case kCMMediaType_Audio:
        {
            vlc_aout_Play(out->audio_out.aout, buffer);
            out->audio_out.last_pts = tick;
            if (out->video_out.last_pts != VLC_TICK_INVALID &&
                out->audio_out.last_pts - out->video_out.last_pts > VLC_TICK_FROM_SEC(3))
                *render_flag = VLC_OUTPUT_RENDER_VIDEO;
            break;
        }
    }
}
