#ifndef VLC_AUDIOSESSION_H
# define VLC_AUDIOSESSION_H 1

#include "vlc-output.h"
#include "vlc-clock.h"

typedef struct vlc_aout vlc_aout;

vlc_aout *vlc_aout_New(vlc_clock_t *clock);

void vlc_aout_Destroy(vlc_aout *aout);

void vlc_aout_Play(vlc_aout *aout, CMSampleBufferRef buffer);
#endif
