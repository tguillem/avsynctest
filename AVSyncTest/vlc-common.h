/*****************************************************************************
 * vlc_common.h: common definitions
 * Collection of useful common types and macros definitions
 *****************************************************************************
 * Copyright (C) 1998-2011 VLC authors and VideoLAN
 *
 * Authors: Samuel Hocevar <sam@via.ecp.fr>
 *          Vincent Seguin <seguin@via.ecp.fr>
 *          Gildas Bazin <gbazin@videolan.org>
 *          Rémi Denis-Courmont
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston MA 02110-1301, USA.
 *****************************************************************************/

#ifndef VLC_COMMON_H
# define VLC_COMMON_H 1

#include <stdlib.h>
#include <stdarg.h>
#include <errno.h>

#include <string.h>
#include <stdio.h>
#include <inttypes.h>
#include <stddef.h>
#include <pthread.h>

#ifndef __cplusplus
# include <stdbool.h>
#endif

#define __MAX(a, b)   (((a) > (b)) ? (a) : (b))

# define likely(p)     __builtin_expect(!!(p), 1)
# define unlikely(p)   __builtin_expect(!!(p), 0)

typedef int64_t vlc_tick_t;
#define CLOCK_FREQ INT64_C(1000000)
#define VLC_TICK_FROM_SEC(sec)   (CLOCK_FREQ * (sec))
#define VLC_TICK_FROM_MS(ms)  ((CLOCK_FREQ / INT64_C(1000)) * (ms))
#define VLC_TICK_FROM_NS(ns)    ((ns)  / (INT64_C(1000000000) / CLOCK_FREQ))
#define NS_FROM_VLC_TICK(vtk)   ((vtk) * (INT64_C(1000000000) / CLOCK_FREQ))
#define VLC_TICK_INVALID INT64_C(0)
#define VLC_TICK_MAX INT64_MAX

#define vlc_tick_from_timespec(tv) \
    (VLC_TICK_FROM_SEC((tv)->tv_sec) + VLC_TICK_FROM_NS((tv)->tv_nsec))

static inline vlc_tick_t vlc_tick_from_samples(int64_t samples, unsigned samp_rate)
{
    return CLOCK_FREQ * samples / samp_rate;
}
static inline int64_t samples_from_vlc_tick(vlc_tick_t t, unsigned samp_rate)
{
    return t * samp_rate / CLOCK_FREQ;
}
static inline vlc_tick_t vlc_tick_from_secf(double secf)
{
    return (vlc_tick_t)(CLOCK_FREQ * secf);
}

vlc_tick_t vlc_tick_now (void);
void vlc_tick_sleep (vlc_tick_t delay);

typedef pthread_mutex_t vlc_mutex_t;
typedef pthread_cond_t vlc_cond_t;

void vlc_mutex_init(vlc_mutex_t *mtx);
void vlc_mutex_destroy(vlc_mutex_t *mtx);
int vlc_mutex_lock(vlc_mutex_t *mtx);
int vlc_mutex_unlock(vlc_mutex_t *mtx);

void vlc_cond_init(vlc_cond_t *cond);
void vlc_cond_destroy(vlc_cond_t *cond);
void vlc_cond_broadcast(vlc_cond_t *cond);
void vlc_cond_signal(vlc_cond_t *cond);
void vlc_cond_wait(vlc_cond_t *cond, vlc_mutex_t *mtx);
int vlc_cond_timedwait(vlc_cond_t *cond, vlc_mutex_t *mtx, vlc_tick_t deadline);

#endif /* !VLC_COMMON_H */
