//
//  SampleBufferRendererView.h
//  AVSyncTest
//
//  Created by Thomas Guillem on 25/01/2023.
//

#ifndef SampleBufferRendererView_h
#define SampleBufferRendererView_h

#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@interface SampleBufferRendererView : UIView
- (AVSampleBufferDisplayLayer *)displayLayer;
@end

#endif /* SampleBufferRendererView_h */
